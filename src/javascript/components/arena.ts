import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { IFighterDetails } from '../helpers/interfaces';

export async function renderArena(selectedFighters: IFighterDetails[]) {
  const root: HTMLElement | null = document.getElementById('root');
  const arena: HTMLElement = createArena(selectedFighters);

  root!.innerHTML = '';
  root!.append(arena);

  try {
    const [firstFighter, secondFighter]: IFighterDetails[] = selectedFighters;
    const winner = await fight(firstFighter, secondFighter);
    showWinnerModal(winner);
  } catch (error) {
    throw error;
  }
}

function createArena(selectedFighters: IFighterDetails[]): HTMLElement {
  const [firstFighter, secondFighter]: IFighterDetails[] = selectedFighters;
  const arena: HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators: HTMLElement = createHealthIndicators(firstFighter, secondFighter);
  const fighters: HTMLElement = createFighters(firstFighter, secondFighter);
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: IFighterDetails, rightFighter: IFighterDetails): HTMLElement {
  const healthIndicators: HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign: HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator: HTMLElement = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator: HTMLElement = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighterDetails, position: 'left' | 'right'): HTMLElement {
  const { name }: { name: string } = fighter;
  const container: HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName: HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar: HTMLElement = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` },
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: IFighterDetails, secondFighter: IFighterDetails): HTMLElement {
  const battleField: HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement: HTMLElement = createFighter(firstFighter, 'left');
  const secondFighterElement: HTMLElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighterDetails, position: 'left' | 'right'): HTMLElement {
  const imgElement: HTMLElement = createFighterImage(fighter);
  const positionClassName: 'arena___right-fighter' | 'arena___left-fighter' =
    position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
