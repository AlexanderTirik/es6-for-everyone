import { controls } from '../../constants/controls';
import { IFighterDetails } from '../helpers/interfaces';

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails): Promise<IFighterDetails> {
  return new Promise((resolve) => {
    let pressed = new Set<string>();

    const leftFighter: CombatFighter = createCombatFighter(firstFighter, 'left');
    const rightFighter: CombatFighter = createCombatFighter(secondFighter, 'right');

    function keyDown(event: KeyboardEvent) {
      if (!event.repeat) pressed.add(event.code);

      getHit(event.code, pressed, event.repeat, leftFighter, rightFighter);

      getCrit(pressed, leftFighter, rightFighter);

      if (rightFighter.health <= 0) {
        Win(firstFighter);
      }
      if (leftFighter.health <= 0) {
        Win(secondFighter);
      }
    }

    document.addEventListener('keydown', keyDown);

    function keyUp(event: KeyboardEvent): void {
      pressed.delete(event.code);
    }

    document.addEventListener('keyup', keyUp);

    function Win(winner: IFighterDetails) {
      document.removeEventListener('keydown', keyDown);
      document.removeEventListener('keyup', keyUp);

      resolve(winner);
    }
  });
}

export function getCrit(pressed: Set<string>, leftFighter: CombatFighter, rightFighter: CombatFighter): void {
  if (controls.PlayerOneCriticalHitCombination.every((code: string): boolean => pressed.has(code))) {
    const resultCrit: boolean = Hit(leftFighter, rightFighter, true);
    if (!resultCrit) {
      controls.PlayerOneCriticalHitCombination.forEach((code: string): boolean => pressed.delete(code));
    }
  }

  if (controls.PlayerTwoCriticalHitCombination.every((code: string): boolean => pressed.has(code))) {
    const resultCrit: boolean = Hit(rightFighter, leftFighter, true);
    if (!resultCrit) {
      controls.PlayerTwoCriticalHitCombination.forEach((code: string): boolean => pressed.delete(code));
    }
  }
}

export function getHit(
  code: string,
  pressed: Set<string>,
  repeat: boolean,
  leftFighter: CombatFighter,
  rightFighter: CombatFighter
): void {
  if (getAttack(code, controls.PlayerOneAttack, pressed, repeat)) {
    Hit(leftFighter, rightFighter);
  }

  if (getAttack(code, controls.PlayerTwoAttack, pressed, repeat)) {
    Hit(rightFighter, leftFighter);
  }
}

export function getAttack(code: string, attack: string, pressed: Set<string>, repeat: boolean): boolean {
  if (code === attack && !pressed.has(controls.PlayerOneBlock) && !pressed.has(controls.PlayerTwoBlock) && !repeat) {
    return true;
  }
  return false;
}

export function getDamage(attacker: CombatFighter, defender: CombatFighter): number {
  const damage: number = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter: CombatFighter): number {
  const attack: number = fighter.attack;
  const criticalHitChance: number = 1 + Math.random();
  const power: number = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: CombatFighter): number {
  const defense: number = fighter.defense;
  const dodgeChance: number = 1 + Math.random();
  const power: number = defense * dodgeChance;
  return power;
}

export function Hit(attacker: CombatFighter, defender: CombatFighter, isCrit: boolean = false): boolean {
  if (isCrit) {
    const dateNow: number = new Date().getTime();
    if (dateNow - attacker.coolDownCrit > 10000) {
      defender.health -= attacker.attack * 2;
      attacker.coolDownCrit = dateNow;
    } else {
      return false;
    }
  } else {
    defender.health -= getDamage(attacker, defender);
  }
  const percentHealth: number = defender.health > 0 ? Math.floor((defender.health / defender.fullHealth) * 100) : 0;
  defender.healthBar.style.width = `${percentHealth}%`;
  return true;
}

interface BattleDetails {
  fullHealth: number;
  coolDownCrit: number;
  healthBar: HTMLElement;
}

type CombatFighter = IFighterDetails & BattleDetails;

export function createCombatFighter(fighter: IFighterDetails, position: 'left' | 'right'): CombatFighter {
  return {
    ...fighter,
    fullHealth: fighter.health,
    coolDownCrit: new Date().getTime() - 11000,
    healthBar: document.getElementById(`${position}-fighter-indicator`) as HTMLElement,
  };
}
