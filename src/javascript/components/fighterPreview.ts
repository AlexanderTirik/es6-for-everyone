import { createElement } from '../helpers/domHelper';
import { IFighterDetails } from '../helpers/interfaces';

export function createFighterPreview(fighter: IFighterDetails, position: "left" | "right"): HTMLElement {
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterName: HTMLElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___name',
    });
    fighterName.innerText = fighter.name;

    const fighterImg: HTMLElement = createFighterImage(fighter);

    if (position == 'right') fighterImg.style.transform = 'scale(-1, 1)';

    const fighterDesc: HTMLElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___description',
    });

    const fighterHealth: HTMLElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___data',
    });
    fighterHealth.innerText = 'Health: ' + fighter.health;
    fighterDesc.append(fighterHealth);

    const fighterAttack: HTMLElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___data',
    });
    fighterAttack.innerText = 'Attack: ' + fighter.attack;
    fighterDesc.append(fighterAttack);

    const fighterDefense: HTMLElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___data',
    });
    fighterDefense.innerText = 'Defense: ' + fighter.defense;
    fighterDesc.append(fighterDefense);

    fighterElement.append(fighterName);
    fighterElement.append(fighterImg);
    fighterElement.append(fighterDesc);
  }

  return fighterElement;
}

export function createFighterImage(fighter: IFighterDetails): HTMLElement {
  const { source, name }: { source: string; name: string } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement: HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
