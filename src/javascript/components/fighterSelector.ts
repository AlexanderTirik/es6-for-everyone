import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { IFighterDetails } from '../helpers/interfaces';
const versusImg = require('../../../resources/versus.png');

export function createFightersSelector() {
  let selectedFighters: IFighterDetails[] = [];

  return async (event: string, fighterId: string) => {
    const fighter: IFighterDetails = await getFighterInfo(fighterId);
    const [playerOne, playerTwo]: IFighterDetails[] = selectedFighters;
    const firstFighter: IFighterDetails = playerOne ?? fighter;
    const secondFighter: IFighterDetails = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map<string, IFighterDetails>();

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
  if (fighterDetailsMap.has(fighterId)) {
    return fighterDetailsMap.get(fighterId) as IFighterDetails;
  } else {
    const fighterInfo: IFighterDetails = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighterInfo);
    return fighterInfo;
  }
}

function renderSelectedFighters(selectedFighters: IFighterDetails[]) {
  const fightersPreview: HTMLElement | null = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo]: IFighterDetails[] = selectedFighters;
  const firstPreview: HTMLElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLElement = createFighterPreview(playerTwo, 'right');
  const versusBlock: HTMLElement = createVersusBlock(selectedFighters);

  fightersPreview!.innerHTML = '';
  fightersPreview!.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFighterDetails[]): HTMLElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = (): void => startFight(selectedFighters);
  const container: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image: HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn: '' | 'disabled' = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: IFighterDetails[]): void {
  renderArena(selectedFighters);
}
