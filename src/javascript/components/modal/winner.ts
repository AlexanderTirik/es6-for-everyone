import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';
import { IFighterDetails, IModalBody } from '../../helpers/interfaces';

export function showWinnerModal(fighter: IFighterDetails): void {
  const imageElement: HTMLElement = createFighterImage(fighter);
  const modalElement: IModalBody = {
    title: `${fighter.name} has won!`,
    bodyElement: imageElement,
  };

  showModal(modalElement);
}
