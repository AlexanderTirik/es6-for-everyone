import { fightersDetails, fighters } from './mockData';
import { IFighterDetails, IDictionary} from './interfaces';

const API_URL: string =
  'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

async function callApi<T>(endpoint: string, method: 'GET' | 'POST' | 'PUT' | 'DELETE'): Promise<T> {
  const url: string = API_URL + endpoint;

  const options: IDictionary = {
    method,
  };

  return useMockAPI
    ? fakeCallApi<T>(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi<T>(endpoint: string): Promise<T> {
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout((): void => (response ? resolve(response as unknown as T) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): IFighterDetails | undefined {
  const start: number = endpoint.lastIndexOf('/');
  const end: number = endpoint.lastIndexOf('.json');
  const id: string = endpoint.substring(start + 1, end);

  return fightersDetails.find((it: IFighterDetails) => it._id === id);
}

export { callApi };
