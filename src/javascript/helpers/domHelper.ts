import { IDomElement } from './interfaces';

export function createElement({ tagName, className, attributes }: IDomElement) {
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  if (attributes) {
    Object.keys(attributes).forEach((key: string): void => element.setAttribute(key, attributes[key]));
  }

  return element;
}
