export interface IFighter {
  _id: string;
  name: string;
  source: string;
}
export interface IFighterDetails {
  _id: string;
  name: string;
  health: number;
  attack: number;
  defense: number;
  source: string;
}

export interface IDictionary {
  [key: string]: string;
}

export interface IDomElement {
  tagName: string;
  className?: string;
  attributes?: IDictionary;
}

export interface IModalBody {
  title: string;
  bodyElement: HTMLElement;
  onClose?: Function;
}
